#include <pthread.h>
#include <stdio.h>
#include <string.h>

/* Un array de dinero en cuentas, indexado por el número de cuenta.  */

float account_balances[100];

/* Transfiere dolares desde la cuenta FROM_ACCT a la cuenta TO_ACCT.  Retorna 0
si la transacción fué exitosa, o 1 si el monto de la cuenta FROM_ACCT es muy pequeño.  */

int process_transaction (int from_acct, int to_acct, float dollars)
{
  int old_cancel_state;

  /* Checkea el monto en FROM_ACCT.  */
  if (account_balances[from_acct] < dollars)
    return 1;

  /* Empieza la sección crítica.  */
  pthread_setcancelstate (PTHREAD_CANCEL_DISABLE, &old_cancel_state);
  /* Transfiere el dinero.  */
  account_balances[to_acct] += dollars;
  account_balances[from_acct] -= dollars;
  /* Finaliza la sección crítica.  */
  pthread_setcancelstate (old_cancel_state, NULL);

  return 0;
}

int main() {

  memset(&account_balances, 0.0, sizeof account_balances);

  account_balances[1] = 500;

  printf("Estado inicial de las cuentas 1 y 2.\n");
  printf("Cuenta 1 --> %.2f USD\n", account_balances[1]);
  printf("Cuenta 2 --> %.2f USD\n", account_balances[2]);

  printf("Llamamos a la función\n");
  process_transaction(1, 2, 230.0);

  printf("Estado final de las cuentas 1 y 2.\n");
  printf("Cuenta 1 --> %.2f USD\n", account_balances[1]);
  printf("Cuenta 2 --> %.2f USD\n", account_balances[2]);  

  return 0;
}