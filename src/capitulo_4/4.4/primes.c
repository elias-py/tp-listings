#include <pthread.h>
#include <stdio.h>

/* Computa números primos (Muy ineficientemente).  Retorna el N número primo, donde 
N es el valor pasado en *ARG.  */

void* compute_prime (void* arg)
{
  int candidate = 2;
  int n = *((int*) arg);

  while (1) {
    int factor;
    int is_prime = 1;

    /* Testea por divisiones sucesivas.  */
    for (factor = 2; factor < candidate; ++factor)
      if (candidate % factor == 0) {
        is_prime = 0;
        break;
      }
    /* Es el primo que estamos buscando?  */
    if (is_prime) {
      if (--n == 0)
        /* Retorna el primo deseado como el valor de retorno del hilo.  */
        return (void*) candidate;
    }
    ++candidate;
  }
  return NULL;
}

int main ()
{
  pthread_t thread;
  int which_prime = 5000;
  int prime;

  /* Comienza el hilo que va a computar el primo número 5000.  */
  pthread_create (&thread, NULL, &compute_prime, &which_prime);
  /* Podemos hacer otras cositas acá mientras  */
  /* Esperamos que el hilo compute el primo deseado y que devuelva el valor.  */
  pthread_join (thread, (void*) &prime);
  /* Imprimimos el primo que nos devolvió el hilo.  */
  printf("The %dth prime number is %d.\n", which_prime, prime);
  return 0;
}