#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>

/*Debemos definir union semun nosotros mismos.  */

union semun {
	int val;
	struct semid_ds *buf;
	unsigned short int *array;
	struct seminfo *__buf;
};


/* Espera un semáforo binario. Bloquea hasta que el valor del semáforo sea positivo, luego reduce el valor en uno.  */

int binary_semaphore_wait (int semid)
{
  struct sembuf operations[1];
  /* Use el primer (y unico) semaforo.  */
  operations[0].sem_num = 0;
  /* Decrementa en 1.  */
  operations[0].sem_op = -1;
  /* Permit undo'ing.  */
  operations[0].sem_flg = SEM_UNDO;
  
  return semop (semid, operations, 1);
}

/* Fija en un semáforo binario: incrementa su valor en uno. Esto vuelve de inmediato.  */

int binary_semaphore_post (int semid)
{
  struct sembuf operations[1];
  /* Use el primer (y unico) semaforo.  */
  operations[0].sem_num = 0;
  /* Incrementa en 1.  */
  operations[0].sem_op = 1;
  /* Permit undo'ing.  */
  operations[0].sem_flg = SEM_UNDO;
  return semop (semid, operations, 1);

}
/* Obtiene un ID de semáforo binario, asignando si es necesario.  */
int binary_semaphore_allocation (key_t key, int sem_flags)
{
	return semget (key, 1, sem_flags);
}

/* Se desasigna un semáforo binario. Todos los usuarios deben haber terminado su uso. Devuelve -1 en caso de falla.  */


int binary_semaphore_deallocate (int semid)
{
	union semun ignored_argument;
	return semctl (semid, 1, IPC_RMID, ignored_argument);
}
/* Inicializa un semáforo binario con un valor de uno.  */
int binary_semaphore_initialize (int semid)
{
	union semun argument;
	unsigned short values[1];
	values[0] = 1;
	argument.array = values;
	return semctl (semid, 0, SETALL, argument);
}


int main(){
	key_t key;
	int id = binary_semaphore_allocation(key, 2);
	binary_semaphore_initialize(id);
	binary_semaphore_wait(id);
	binary_semaphore_post (id);
	binary_semaphore_deallocate(id);
}
