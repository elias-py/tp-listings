#include <fcntl.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

char* read_from_file (const char* filename, size_t length)
{
  char* buffer;
  int fd;
  ssize_t bytes_read;

  /* Asignar el buffer.  */
  buffer = (char*) malloc (length);
  if (buffer == NULL)
    return NULL;
  /* Abre el archivo.  */
  fd = open (filename, O_RDONLY);
  if (fd == -1) {
    /* Error al abrir. Desasignar el buffer antes de regresar.  */
    free (buffer);
    return NULL;
  }
  /* Leer los datos.  */
  bytes_read = read (fd, buffer, length);
  if (bytes_read != length) {
    /* Lectura fallida. Desasigne el buffer y cierre fd antes de regresar.  */
    free (buffer);
    close (fd);
    return NULL;
  }
  /* Todo esta bien. Cierre el archivo y devuelva el buffer.  */
  close (fd);
  return buffer;
}
int main () 
{
    size_t f_size;  
    char * f_data;
    f_data = read_from_file(f_data, 0);
}
