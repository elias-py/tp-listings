#include <stdio.h>
#include <stdlib.h>

int main ()
{
  char* server_name = getenv ("SERVER_NAME");
  if (server_name == NULL) 
    /* La variable de entorno SERVER_NAME no se establecio. Utilizar por
    defecto.  */
    server_name = "server.my-company.com";

  printf ("acceso al servidor %s\n", server_name);
  /* Acceda al servidor aqui...  */

  return 0;
}
