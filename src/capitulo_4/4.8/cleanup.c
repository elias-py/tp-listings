#include <stdlib.h>
#include <pthread.h>
#include <stdio.h>

/* Asigna un búfer termporal.  */

void* allocate_buffer (size_t size)
{
  return malloc (size);
}

/* Desasigna un búfer temporal  */

void deallocate_buffer (void* buffer)
{
  free (buffer);
  printf("entra en deallocate_buffer\n");
}

void do_some_work ()
{
  /* Asigna un búfer temporal.  */
  void* temp_buffer = allocate_buffer (1024);

  printf("%lu\n", sizeof(temp_buffer));

  /* Registra un controlador de limpieza para este búfer, para desasignarlo en el
     caso de que el hilo termine o sea cancelado.  */
  pthread_cleanup_push (deallocate_buffer, temp_buffer);

  /* Hacer algún trabajo aquí, podría ser llamar a pthread_exit o podría ser 
     cancelado...  */
  pthread_exit(temp_buffer);
  /* Dar de baja el controlador de limpieza.  Como pasamos un valor distinto de cero,
     esto en realidad realiza la limpieza llamando a
     deallocate_buffer.  */
  pthread_cleanup_pop (1);
}

int main() {

  do_some_work();

  return 0;

}