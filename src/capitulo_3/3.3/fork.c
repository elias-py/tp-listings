/***********************************************************************
* Code listing from "Advanced Linux Programming," by CodeSourcery LLC  *
* Copyright (C) 2001 by New Riders Publishing                          *
* See COPYRIGHT for license information.                               *
***********************************************************************/

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

int main ()
{
  pid_t child_pid;

  printf ("El ID del proceso del programa principal es %d\n", (int) getpid ());

  child_pid = fork ();
  if (child_pid != 0) {
    printf ("Este es el proceso padre,con id %d\n", (int) getpid ());
    printf ("El ID del proceso del niño es %d\n", (int) child_pid);
  }
  else 
    printf ("Este es el proceso hijo, con id %d\n", (int) getpid ());

  return 0;
}
