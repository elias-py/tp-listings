#include <pthread.h>
#include <stdio.h>

/* Imprime x's en el stderr. El parámetro no es usado. No retorna nada.  */

void* print_xs (void* unused)
{
  while (1) 
    fputc ('x', stderr);
  return NULL;
}

/* El programa principal.  */

int main ()
{
  /* Crea una variable que pueda almacenar el ID de un hilo */
  pthread_t thread_id;
  /* Crea un nuevo hilo. El nuevo hilo va a correr la funcion
  print_xs  */
  pthread_create (&thread_id, NULL, &print_xs, NULL);
  /* Imprime o's de seguido en el stderr.  */
  while (1) 
    fputc ('o', stderr);
  return 0;
}