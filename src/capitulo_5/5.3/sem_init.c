#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>

/*Debemos definir union semun nosotros mismos.  */

union semun {
  int val;
  struct semid_ds *buf;
  unsigned short int *array;
  struct seminfo *__buf;
};

/* Inicializa un semáforo binario con un valor de uno.  */

int binary_semaphore_initialize (int semid)
{
  union semun argument;
  unsigned short values[1];
  values[0] = 1;
  argument.array = values;
  return semctl (semid, 0, SETALL, argument);
}
int binary_semaphore_allocation (key_t key, int sem_flags)
{
	return semget (key, 1, sem_flags);
}

/* Se desasigna un semáforo binario. Todos los usuarios deben haber terminado su uso. Devuelve -1 en caso de falla.  */

int binary_semaphore_deallocate (int semid)
{
	union semun ignored_argument;
	return semctl (semid, 1, IPC_RMID, ignored_argument);
}

/* se asigna valores a los semaforos*/
int main(){
	key_t key;
	int id = binary_semaphore_allocation(key, 9);
	binary_semaphore_initialize(id);
	binary_semaphore_deallocate(id);
}
