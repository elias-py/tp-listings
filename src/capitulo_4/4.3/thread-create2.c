#include <pthread.h>
#include <stdio.h>

/* Parámetros para imprimir en la función char_print.  */

struct char_print_parms
{
  /* El caracter a imprimir.  */
  char character;
  /* El número de veces que se va a imprimir.  */
  int count;
};

/* Imprime un número de caracteres al stderr, dados por PARÁMETROS
   el cual es un puntero a una estructura char_print_parms.  */

void* char_print (void* parameters)
{
  /* Castea el puntero al tipo de dato correcto.  */
  struct char_print_parms* p = (struct char_print_parms*) parameters;
  int i;

  for (i = 0; i < p->count; ++i)
    fputc (p->character, stderr);
  return NULL;
}

/* El programa principal.  */

int main ()
{
  /* Variables que almacenen los id de los hilos a crear */
  pthread_t thread1_id;
  pthread_t thread2_id;
  /* Variables que almacenen las estructuras de los argumentos que vamos a pasar */
  struct char_print_parms thread1_args;
  struct char_print_parms thread2_args;

  /* Crea un nuevo hilo que va a imprimir 30000 x's.  */
  thread1_args.character = 'x';
  thread1_args.count = 30000;
  pthread_create (&thread1_id, NULL, &char_print, &thread1_args);

  /* Crea un nuevo hilo que va a imprimir 20000 o's.  */
  thread2_args.character = 'o';
  thread2_args.count = 20000;
  pthread_create (&thread2_id, NULL, &char_print, &thread2_args);
  
  /* Asegurarse de que el primer hilo haya finalizado.  */
  pthread_join (thread1_id, NULL);
  /* Asegurarse de que el segundo hilo haya finalizado.  */
  pthread_join (thread2_id, NULL);

  /* Ahora podemos terminar y retornar de forma segura. */
  return 0;
}