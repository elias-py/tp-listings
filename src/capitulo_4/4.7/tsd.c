#include <pthread.h>
#include <stdio.h>

/* El key usado para asociar un archivo de registro de punteros con cada hilo.  */
static pthread_key_t thread_log_key;

/* Write MESSAGE to the log file for the current thread.  */

void write_to_thread_log (const char* message)
{
  FILE* thread_log = (FILE*) pthread_getspecific (thread_log_key);
  fprintf (thread_log, "%s\n", message);
}

/* Cierra el registro de punteros de archivos THREAD_LOG.  */

void close_thread_log (void* thread_log)
{
  fclose ((FILE*) thread_log);
}

void* thread_function (void* args)
{
  char thread_log_filename[20];
  FILE* thread_log;

  /* Genera el nombre del archivo para este registro de archivos de hilo.  */
  sprintf (thread_log_filename, "thread%d.log", (int) pthread_self ());
  /* Abre el registro de archivos.  */
  thread_log = fopen (thread_log_filename, "w");
  /* Guarda el puntero del archivo en un hilo específico bajo thread_log_key.  */
  pthread_setspecific (thread_log_key, thread_log);

  write_to_thread_log ("Thread starting.");
  /* Hace algún trabajo aquí... */

  return NULL;
}

int main ()
{
  int i;
  pthread_t threads[5];

  /* Crea un key para los punteros de archivos de registros de subprocesos asociados en
     los datos específicos del hilo. Usar close_thread_log para limpiar el archivo de punteros.  */
  pthread_key_create (&thread_log_key, close_thread_log);
  
  /* Crea los hilos para hacer el los trabajos.  */
  for (i = 0; i < 5; ++i)
    pthread_create (&(threads[i]), NULL, thread_function, NULL);
  
  /* Espera a todos los hilos para finalizar.  */
  for (i = 0; i < 5; ++i)
    pthread_join (threads[i], NULL);

  return 0;
}