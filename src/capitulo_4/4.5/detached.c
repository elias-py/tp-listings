#include <pthread.h>
#include <stdio.h>

void* thread_function (void* thread_arg)
{
  /* Hace algo acá..  */
  int i;
  char letra = 'x';
  for (i=0; i<10000; i++)
    fputc(letra, stderr);

  return NULL;
}

int main ()
{
  pthread_attr_t attr;
  pthread_t thread;

  pthread_attr_init (&attr);
  pthread_attr_setdetachstate (&attr, PTHREAD_CREATE_DETACHED);
  pthread_create (&thread, &attr, &thread_function, NULL);
  pthread_attr_destroy (&attr);

  /* Hace algo acá...  */
  fputc('a', stderr);
  /* No hay necesidad de esperar al hilo.  */
  /* Una vez que llega al return 0 se cancelan todos los hilos */
  return 0;
}