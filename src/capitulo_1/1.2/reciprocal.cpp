#include <cassert>
#include "../1.3/reciprocal.hpp"

double reciprocal (int i) {
  // Deberia ser distinto de cero.
  assert (i != 0);
  return 1.0/i;
}
