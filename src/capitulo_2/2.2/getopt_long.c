#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>

/* El nombre de este programa.  */
const char* program_name;

/* Imprime la informacion de uso de este programa en STEAM (normalmente
stdout o stderr) y sale del programa con EXIT_CODE. Sin return.*/

void print_usage (FILE* stream, int exit_code)
{
  fprintf (stream, "Uso:  %s opciones [ inputfile ... ]\n", program_name);
  fprintf (stream,
           "  -h  --help             Muestra esta informacion de uso.\n"
           "  -o  --output filename  Escribe salida en archivo.\n"
           "  -v  --verbose          Imprimir mensajes detallados.\n");
  exit (exit_code);
}

/* Punto de entrada del programa principal. ARGC contiene numero de lista 
de argumentos;ARGV es una serie de punteros para ellos.  */

int main (int argc, char* argv[])
{
  int next_option;

  /* Una cadena que enumera letras validas de opciones cortas.  */
  const char* const short_options = "ho:v";
  /* Una matriz que describe opciones largas validas.  */
  const struct option long_options[] = {
    { "help",     0, NULL, 'h' },
    { "output",   1, NULL, 'o' },
    { "verbose",  0, NULL, 'v' },
    { NULL,       0, NULL, 0   }   /* Requerido al final de la matriz.  */
  };

  /* El nombre del archivo para recibir la salida del programa, o NULL para salida estandar.  */
  const char* output_filename = NULL;
  /* Si se muestran mensajes detallados.  */
  int verbose = 0;

  /* Recuerdael nombre del programa, para incorporar en los mensajes.
    El nombre se almacena en argv[0].  */
  program_name = argv[0];

  do {
    next_option = getopt_long (argc, argv, short_options,
                               long_options, NULL);
    switch (next_option)
    {
    case 'h':   /* -h or --help */
      /* El usuario ha solicitado informacion de uso. Imprimirlo al estandar
    de salida y salir con el codigo de salida cero (Terminacion normal).  */
      print_usage (stdout, 0);

    case 'o':   /* -o or --output */
      /* Esta opcion toma un argumento, el nombre del archivo de salida.  */
      output_filename = optarg;
      break;

    case 'v':   /* -v or --verbose */
      verbose = 1;
      break;

    case '?':   /* El usuario especifico una opcion no valida.  */
      /* Imprimir informacion de uso al error estandar y salir con exit
    codigo uno (que indica una terminacion anormal).  */
      print_usage (stderr, 1);

    case -1:    /* Hecho con opciones.  */
      break;

    default:    /* Algo más inesperado.  */
      abort ();
    }
  }
  while (next_option != -1);

  /* Hecho con opciones. OPTIND apunta al primer argumento de no opcion
    Para fines de demostracion, imprimalos si la opcion detallada era
    especificado.  */
  if (verbose) {
    int i;
    for (i = optind; i < argc; ++i) 
      printf ("Argumento: %s\n", argv[i]);
  }

  /* El programa principal va aqui.  */

  return 0;
}
