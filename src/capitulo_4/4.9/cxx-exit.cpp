#include <pthread.h>
#include <stdio.h>

bool should_exit_thread_immediately () {
  return true;
};

class ThreadExitException
{
public:
  /* Crea una señal de excepción de salida del hilo con un RETURN_VALUE.  */
  ThreadExitException (void* return_value)
    : thread_return_value_ (return_value)
  {
  }

  /* Saliendo del hilo, usando el valor del retorno proveido por el
  constructor.  */
  void* DoThreadExit ()
  {
    pthread_exit (thread_return_value_);
  }

private:
  /* El valor de retorno que será usado cuando salga el hilo.  */
  void* thread_return_value_;
};

void do_some_work ()
{
  while (1) {
    /* Hacer algo útil acá...  */
    for (int i=0; i<10; i++)
      printf("%d\n", i+1);

    if (should_exit_thread_immediately ()) 
      throw ThreadExitException (/* valor de retorno del hilo = */ NULL);
  }
}

void* thread_function (void*)
{
  try {
    do_some_work ();
  }
  catch (ThreadExitException ex) {
    /* Alguna función indicando que debemos salir del hilo.  */
    ex.DoThreadExit ();
  }
  return NULL;
}

int main() {

  pthread_t thread_id;

  pthread_create(&thread_id, NULL, &thread_function, NULL);
  pthread_join(thread_id, NULL);

  return 0;
}