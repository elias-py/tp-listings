#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

/* Genera un proceso hijo que ejecuta un nuevo programa.  PROGRAM es el nombre
   del programa a ejecutar, se buscara la ruta para este programa.
   ARG_LIST es una lista terminada en NULL de cadenas de caracteres para ser
   pasado como la lista de argumentos del programa. Retorna el ID de proceso de
   el proceso generado.  */

int spawn (char* program, char** arg_list)
{
  pid_t child_pid;

  /* Duplicar este proceso.  */
  child_pid = fork ();
  if (child_pid != 0)
    /* Este es el porceso padre.  */
    return child_pid;
  else {
    /* Ahora ejecuta PROGRAM, buscandolo en la ruta.  */
    execvp (program, arg_list);
    /* La funcion execvp regresa solo si ocurre un error.  */
    fprintf (stderr, "se produjo un error en execvp\n");
    abort ();
  }
}

int main ()
{
  /* La lista de argumentos para pasar al comando "ls".  */
  char* arg_list[] = {
    "ls",     /* argv[0], el nombre del programa.  */
    "-l", 
    "/",
    NULL      /* La lista de argumentos debe terminanr con un NULL.  */
  };

  /* Genera un proceso hijo que ejecuta el comando "ls". Ignora el
     ID de proceso hijo devuelto.  */
  spawn ("ls", arg_list); 

  printf ("hecho con el programa principal\n");

  return 0;
}
