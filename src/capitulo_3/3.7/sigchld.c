#include <signal.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>

sig_atomic_t child_exit_status;

void clean_up_child_process (int signal_number)
{
  /* Limpiar el proceso hijo.  */
  int status;
  wait (&status);
  /* Almacena su estado de salida en una variable global.  */
  child_exit_status = status;
}

int main ()
{
  /* Manejar SIGCHLD llamando a clean_up_child_process.  */
  struct sigaction sigchld_action;
  memset (&sigchld_action, 0, sizeof (sigchld_action));
  sigchld_action.sa_handler = &clean_up_child_process;
  sigaction (SIGCHLD, &sigchld_action, NULL);

  /* Ahora hago cosas, incluso bifurcar un proceso secundario.  */
  /* ...  */

  return 0;
}
