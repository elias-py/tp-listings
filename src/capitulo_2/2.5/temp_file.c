#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

/* Un identificador para un archivo temporal creado con write_temp_file.
   En esta emplementacion, es solo un descriptor de archivo.  */
typedef int temp_file_handle;

/* Escribe bytes de LONGITUD desde BUFFER en un archivo temporal. El
   archivo temporal se desvincula de inmediato. Devuelve un identificador
   a archivo temporal.     */

temp_file_handle write_temp_file (char* buffer, size_t length)
{
  /* Crea el nombre del archivo y el archivo. El XXXXXX sera remplazado con 
    caracteres que hacen que el nombre del archivo sea unico. */
  char temp_filename[] = "/tmp/temp_file.XXXXXX";
  int fd = mkstemp (temp_filename);
  /* Desvincular el archivo inmediatamente, para que se elimine cuando el
     el descriptor de archivo está cerrado.  */
  unlink (temp_filename);
  /* Escriba primero el numero de bytes en el archivo.  */
  write (fd, &length, sizeof (length));
  /* Ahora escribe los datos en si.  */
  write (fd, buffer, length);
  /* Utilice el descriptor de archivo como el identificador para
    el archivo temporal.  */
  return fd;
}

/* Lee el contenido de un archivo temporal TEMP_FILE creado con
   write_temp_file.  El valor de retorno en un bufer recientemente 
   asignado de esos contenidos,que la persona que llama debe tratar 
    de forma gratuita.
   *LENGTH se establece en el tamaño de los contenidos, en bytes.
   Se elimina el archivo temporal.  */

char* read_temp_file (temp_file_handle temp_file, size_t* length)
{
  char* buffer;
  /* el identificador TEMP_FILE es un descriptor de archivo para le archivo
    temporal.  */
  int fd = temp_file;
  /* Rebobinar hasta el comienzo del archivo.  */
  lseek (fd, 0, SEEK_SET);
  /* Leer el tamaño de los datos en el archivo temporal.  */
  read (fd, length, sizeof (*length));
  /* Asignar un buffer y leer los datos.  */
  buffer = (char*) malloc (*length);
  read (fd, buffer, *length);
  /* Cerrar el descriptor de archivo, lo que hará que el archivo temporal
     se vaya.  */
  close (fd);
  return buffer;
}

int main(int argc, char* argv[])
{
    char* read_buffer;
    size_t length;
    temp_file_handle fd;
    if(argc > 1){
        length = strlen(argv[1]);
        fd = write_temp_file(argv[1], length);
        read_buffer = read_temp_file(fd, &length);
    }
    printf("Output: %s\n", read_buffer);
    free(read_buffer);
    return 0;
}
